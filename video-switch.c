#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
#include "tilcdc_drm.h"

#define CARD		"/dev/dri/card0"
#define LCD_CONN	7
#define HDMI_CONN	11

const char *conn_names[] = {
	"Unknown",
	"VGA",
	"DVI-I",
	"DVI-D",
	"DVI-A",
	"Composite",
	"SVIDEO",
	"LVDS",
	"Component",
	"DIN",
	"DP",
	"HDMI-A",
	"HDMI-B",
	"TV",
	"eDP",
	"Virtual",
	"DSI"
};

int get_conn_by_name(char *name)
{
	if (!strcasecmp(name, "hdmi"))
		return HDMI_CONN;
	else if (!strcasecmp(name, "lcd"))
		return LCD_CONN;

	return -1;
}

const char *get_conn_name(int conntype)
{
	if (conntype == HDMI_CONN)
		return "hdmi";
	else if (conntype == LCD_CONN)
		return "lcd";

	return conn_names[conntype];
}

int list_modes(int fd, drmModeRes *res)
{
	int i, j;
	drmModeConnector *conn;
	for (i = 0; i < res->count_connectors; ++i) {
		conn = drmModeGetConnector(fd, res->connectors[i]);

		for (j = 0; j < conn->count_modes; j++) {
			/*
			 * Some modes only differ in the pixel clock.
			 *
			 * Since we're not specifying it and leaving the
			 * driver to fixup the mode as it sees fit, there's
			 * no reason to list modes that don't differ in 
			 * resolution or frequency.
			 *
			 * We take advantage on the fact that the modes are
			 * ordered.
			 */
			if (j > 0
			 && conn->modes[j].hdisplay == conn->modes[j-1].hdisplay
			 && conn->modes[j].vdisplay == conn->modes[j-1].vdisplay
			 && conn->modes[j].vrefresh == conn->modes[j-1].vrefresh)
				continue;

			printf("%s %ix%i@%i\n",
				get_conn_name(conn->connector_type),
				conn->modes[j].hdisplay,
				conn->modes[j].vdisplay,
				conn->modes[j].vrefresh);
		}
	}

	return 0;
}

void usage(char *name)
{
	fprintf(stderr, "usage: %s set hdmi <xres> <yres> [<bpp> <refresh>]\n", name);
	fprintf(stderr, "       %s set lcd [bpp]\n", name);
	fprintf(stderr, "       %s list\n", name);
}

int main(int argc, char **argv)
{
	int fd;
	struct tilcdc_changemode arg;
	int ret;

	if (argc < 2) {
		usage(argv[0]);
		return 1;
	}

	if (!strcmp(argv[1], "set")) {
		if (argc < 3) {
			usage(argv[0]);
			return 1;
		}

		arg.connector = get_conn_by_name(argv[2]);
		if (arg.connector == HDMI_CONN && argc != 7 && argc != 5) {
			usage(argv[0]);
			return 1;
		} else if (arg.connector == LCD_CONN && argc != 3 && argc != 4) {
			usage(argv[0]);
			return 1;
		} else if (arg.connector == -1) {
			fprintf(stderr, "invalid connector name \"%s\"\n", argv[2]);
			exit(1);
		}

		/* open the DRM device */
		fd = open(CARD, O_RDWR);
		if (fd < 0) {
			fprintf(stderr, "Failed to open card\n");
			exit(1);
		}

		if (arg.connector == LCD_CONN) {
			arg.width	= 480;
			arg.height	= 272;
			arg.bpp		= argc == 4 ? atoi(argv[3]) : 0;
			arg.refresh	= 0;
		} else { /* HDMI_CONN */
			arg.width	= atoi(argv[3]);
			arg.height	= atoi(argv[4]);

			if (argc == 7) {
				arg.bpp		= atoi(argv[5]);
				arg.refresh	= atoi(argv[6]);
			} else {
				arg.bpp		= 0;
				arg.refresh	= 0;
			}
		}

		ret = ioctl(fd, DRM_IOCTL_TILCDC_CHANGEMODE, &arg);

		if (ret) {
			fprintf(stderr, "Mode switch failed!\n");
			return 2;
		}

		close(fd);
	} else if (!strcmp(argv[1], "list")) {
		drmModeRes *res;

		if (argc != 2) {
			usage(argv[0]);
			return 1;
		}

		fd = open(CARD, O_RDWR);
		if (fd < 0) {
			fprintf(stderr, "Failed to open card\n");
			exit(1);
		}

		res = drmModeGetResources(fd);
		list_modes(fd, res);
		drmModeFreeResources(res);

		return 0;
	} else {
		usage(argv[0]);
		return 1;
	}

	return 0;
}
