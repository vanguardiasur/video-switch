#ifndef _TILCDC_DRM_H_
#define _TILCDC_DRM_H_

#define DRM_TILCDC_CHANGEMODE 0x40

struct tilcdc_changemode {
	uint32_t connector;
	uint32_t height;
	uint32_t width;
	uint32_t refresh;
	uint8_t bpp;
} __attribute__((packed));

#define DRM_IOCTL_TILCDC_CHANGEMODE	0x40116480

#endif
