.PHONY: all clean re test

NAME=video-switch
all: $(NAME)

%: %.c
	$(CC) -ldrm -Wno-sign-compare -Wall -Wextra $< -o $@

clean:
	rm -f $(NAME)

re: clean all
