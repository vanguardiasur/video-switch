#!/bin/sh

test()
{
	w=$1
	h=$2
	bpp=$3
	conn=$4

	if [ "$conn" == "11" ]; then
		echo 0 > /sys/class/backlight/backlight/brightness
	else
		echo 7 > /sys/class/backlight/backlight/brightness
	fi

	video-switch $w $h $bpp $conn
	sleep 2
	fb-test
	sleep 5
}

while [ 1 ]; do
	test 480 272 16 7
	test 640 480 32 11
	test 800 600 32 11
	test 1024 768 32 11
	test 1280 720 32 11
	test 1920 1080 32 11
done
